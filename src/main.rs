use http::Method;
use lambda_http::{http, run, service_fn, Body, Error, Request, RequestExt, Response};
use mini_project5::{handle_delete, handle_get, handle_put};
use tracing::{debug, info};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

// handle incoming requests
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let key: String = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("key"))
        .unwrap_or("")
        .to_string();
    debug!("Key = {}", key);

    let date: String = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("date"))
        .unwrap_or("")
        .to_string();
    debug!("Date = {}", date);

    let body: String = String::from_utf8_lossy(event.body()).to_string();

    info!("Handling request...");
    let message = match *event.method() {
        Method::GET => handle_get(key, date).await,
        Method::PUT => handle_put(key, body).await,
        Method::DELETE => handle_delete(key, date).await,
        _ => "Unsupported HTTP Request\n".to_string(),
    };

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body(message.into())
        .map_err(Box::new)?;
    info!("Request handled.");
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
