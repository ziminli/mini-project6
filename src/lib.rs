use aws_sdk_dynamodb::{types::AttributeValue, Client};
use chrono::prelude::*;
use tracing::{debug, info};

// query dynamodb for the item with the partition key "Key" and the sort key "CreationDate"
async fn query_key(key: String) -> String {
    let dynamodb_client =
        Client::new(&aws_config::load_defaults(aws_config::BehaviorVersion::latest()).await);
    let resp = dynamodb_client
        .query()
        .table_name("mini-project5-database")
        .key_condition_expression("#Key = :key")
        .expression_attribute_names("#Key", "Key")
        .expression_attribute_values(":key", AttributeValue::S(key))
        .send()
        .await;

    match resp {
        Ok(items) => {
            let items = items.items.unwrap();
            let mut all_data = String::new();
            for item in items {
                let date = item.get("CreationDate").unwrap().as_s().unwrap();
                let data = item.get("Data").unwrap().as_s().unwrap();
                let item = format!("[{}] - {}\n", date, data);
                all_data.push_str(&item);
            }
            all_data
        }
        Err(e) => {
            info!("[query_key]: Error: {} is returned from the database.", e);
            format!("Error: {}\n", e).to_string()
        }
    }
}

// get the item in dynamodb with the partition key "Key" and sort key "CreationDate"
async fn query_key_date(key: String, date: String) -> String {
    let dynamodb_client =
        Client::new(&aws_config::load_defaults(aws_config::BehaviorVersion::latest()).await);
    let resp = dynamodb_client
        .get_item()
        .table_name("mini-project5-database")
        .key("Key", AttributeValue::S(key))
        .key("CreationDate", AttributeValue::S(date))
        .send()
        .await;

    match resp {
        Ok(item) => {
            let item = item.item.unwrap();
            let data = item.get("Data").unwrap();
            let data = data.as_s().unwrap().clone().to_owned() + "\n";
            data.to_string()
        }
        Err(e) => {
            info!(
                "[query_key_date]: Error: {} is returned from the database.",
                e
            );
            format!("Error: {}\n", e).to_string()
        }
    }
}

// get the item based on the provided query strings
pub async fn handle_get(key: String, date: String) -> String {
    if date.is_empty() {
        debug!("Only key is provided.");
        query_key(key).await
    } else {
        debug!("Both key and date are provided.");
        query_key_date(key, date).await
    }
}

// creaet a new item in dynamodb with the partition key "key" and the value "data"
pub async fn handle_put(key: String, data: String) -> String {
    let dynamodb_client =
        Client::new(&aws_config::load_defaults(aws_config::BehaviorVersion::latest()).await);
    let resp = dynamodb_client
        .put_item()
        .table_name("mini-project5-database")
        .item("Key", AttributeValue::S(key))
        .item(
            "CreationDate",
            AttributeValue::S(Utc::now().date_naive().to_string()),
        )
        .item("Data", AttributeValue::S(data))
        .send()
        .await;

    match resp {
        Ok(_) => "Success\n".to_string(),
        Err(e) => {
            info!("[handle_put]: Error: {} is returned from the database.", e);
            format!("Error: {}\n", e).to_string()
        }
    }
}

// delete the item in dynamodb with the partition key "Key" and sort key "CreationDate"
pub async fn handle_delete(key: String, date: String) -> String {
    let dynamodb_client =
        Client::new(&aws_config::load_defaults(aws_config::BehaviorVersion::latest()).await);
    let resp = dynamodb_client
        .delete_item()
        .table_name("mini-project5-database")
        .key("Key", AttributeValue::S(key))
        .key("CreationDate", AttributeValue::S(date))
        .condition_expression("attribute_exists(Key)")
        .condition_expression("attribute_exists(CreationDate)")
        .send()
        .await;

    match resp {
        Ok(_) => "Success\n".to_string(),
        Err(e) => {
            info!(
                "[handle_delete]: Error: {} is returned from the database.",
                e
            );
            format!("Error: {}. No match records.\n", e).to_string()
        }
    }
}
