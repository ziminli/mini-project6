all: format lint build deploy

watch:
	cargo lambda watch
	
invoke:
	curl -X GET https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5?key=moon
	
format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet
	
build:
	cargo lambda build --release --arm64
	
deploy:
	cargo lambda deploy --enable-function-url --iam-role arn:aws:iam::536768436271:role/cargo-lambda-role-2628154b-532d-4e25-b75b-54b1dc79c263