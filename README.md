# Mini-Project6
A serverless online diary and memo that supports record reading, writing, same-day updating, and deleting. The application is developed using Rust with [Cargo Lambda](https://www.cargo-lambda.info/), interfaced with an AWS API Gateway, connected to a [DynamoDB](https://aws.amazon.com/dynamodb/), deployed on [AWS Lambda](https://aws.amazon.com/lambda/), and enabled AWS [X-Ray tracing](https://aws.amazon.com/xray/) and [CloudWatch](https://aws.amazon.com/cloudwatch/) logging.   

This mini-project is based on the [previous one](https://gitlab.com/ziminli/mini-project5) with logging and AWS X-Ray tracing. 

<img src="assets/lambda_arch.jpg" alt="lambda structure" width=500 height=240>

## Record Structure
A record (memo or diary) is composed of three parts: 

1. __Title (Key)__. This is required for every record and it cannot be empty.
2. __Creation Date__. This is automatically created for every record when it is created.
3. __Body Content (Data)__. The actual content.

Here are some example records:

<img src="assets/db_instances.jpg" alt="database records" width=640 height=320>

## Logging and Monitoring
The program uses the tracing and tracing subscriber crates to perform logging. Additionally, AWS X-Ray and CloudWatch are used for monitoring and analyzing the application. 

### X-Ray Tracing
![AWS X-Ray](assets/xray_map.jpg)

This shows the traces and trace map of the requests received by the serverless application. 

### CloudWatch Logs
![AWS CloudWatch](assets/log.jpg)

The above is one of the logs produced by a single request-handling activity, including both the ones logged by the application and the ones CloudWatch creates. 


## Usage
The service API endpoint is at: https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5

Querying the service is done by sending HTTP requests with query string parameter(s) and potentially body content.

curl and HTTP API builder like [Postman](https://www.postman.com/) are convenient tools to use the service. 

### Read
Reading records is done by sending GET requests. There are two ways to read records:

1. Querying only the title (Key). This will return a list of all the records whose titles match the given one. If there are no matching records, nothing will be returned. Syntax using curl:

`curl -X GET 'https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5?key=<title>'`

With the formerly shown records, running the command with the title = "moon" will get:

![read_key](assets/read_key.jpg)

2. Query both the title and creation date. If there is a matching record, the record will be returned. If does not exist, then nothing will be returned. Syntax using curl:

`curl -X GET 'https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5?key=<title>&date=<creation_date>'`

With the formerly shown records, running the command with the title = "sky" and creation date = "2024-02-24" will get:

![read_key_date](assets/read_key_date.jpg)

### Write/Create
To write/create a new record, a PUT request should be sent. A title must be provided as a query string parameter and the actual content should be in the body section. The creation date will be automatically stamped. Note: UTC time is used to ensure a consistent time recording. Syntax using curl:

`curl -X PUT 'https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5?key=<title>' -H '<Content-Type>' -d '<content>'`

For instance, creating a record with the title "Happy Day" and plain text body "This is a happy day." using curl:

`curl -X PUT 'https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5?key=Happy%20Day' -H 'Content-Type: text/plain' -d 'This is a happy day.'`

If it succeeds, a "Success" message will be returned. Otherwise, an error message will be returned. It can be verified that the exact record has been created from the DynamoDB GUI:

![write_success](assets/write_success.jpg)

### Same-day Update
Same-day updates can be achieved using the exact method as creating a new one. Creating a record with the same title on the same day (note, UTC time) will overwrite the existing one. 

### Delete
To delete a record, a DELETE request should be sent. Both the title and creation date should be specified to successfully delete the record if it exists. Syntax using curl:

`curl -X DELETE 'https://a5qlgfec9b.execute-api.us-east-1.amazonaws.com/default/mini-project5?key=<title>&date=<creation_date>'`

If it succeeds, a "Success" message will be returned. Otherwise, an error message will be returned.

## API Gateway
AWS API Gateway is used mainly to serve as a proxy. It filters many malformed requests before reaching the lambda function and automatically provides instructional error messages. 

## DynamoDB
DynamoDB is a key-value based NoSQL database. It perfectly fits the need for this application and as an AWS service, it works seamlessly with AWS Lambda.
 
